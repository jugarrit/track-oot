// frameworks
import Context from "/emcJS/data/Context.js";

const GlobalContext = new Context();
/*global globalThis*/
globalThis.GlobalContext = GlobalContext;
export default GlobalContext;
