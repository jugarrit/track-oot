// frameworks
import Enum from "/emcJS/data/Enum.js";


export default new Enum(
    "UNAVAILABLE",
    "POSSIBLE",
    "AVAILABLE",
    "OPENED"
);
