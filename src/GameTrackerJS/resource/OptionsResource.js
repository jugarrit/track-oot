import JSONCResourceFile from "../data/JSONCResourceFile.js";

export default await JSONCResourceFile.create("/database/options.json");
