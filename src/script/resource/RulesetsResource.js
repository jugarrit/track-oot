import JSONCResourceFile from "/GameTrackerJS/data/JSONCResourceFile.js";

export default await JSONCResourceFile.create("/database/rulesets.json");
