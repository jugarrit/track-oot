// frameworks
import Template from "/emcJS/util/html/Template.js";
import GlobalStyle from "/emcJS/util/html/GlobalStyle.js";
import CustomElement from "/emcJS/ui/CustomElement.js";
import "/emcJS/ui/input/Option.js";


// GameTrackerJS
import ItemStates from "/GameTrackerJS/state/item/StateManager.js";
import StateDataEventManager from "/GameTrackerJS/ui/mixin/StateDataEventManager.js";
import UIRegistry from "/GameTrackerJS/registry/UIRegistry.js";
// Track-OOT
import "/script/state/item/KeyState.js";
import "./Item.js";

const TPL = new Template(`
<slot id="slot">
</slot>
`);

const STYLE = new GlobalStyle(`
* {
    position: relative;
    box-sizing: border-box;
    -webkit-user-select: none;
    -moz-user-select: none;
    user-select: none;
}
:host {
    display: inline-flex;
    width: 40px;
    height: 40px;
    cursor: pointer;
}
#slot {
    width: 100%;
    height: 100%;
    font-size: 1em;
    --halign: center;
    --valign: center;
}
::slotted(:not([value])),
::slotted([value]:not(.active)) {
    display: none !important;
}
::slotted([value]) {
    display: inline-flex;
    align-items: var(--valign, center);
    justify-content: var(--halign, center);
    width: 100%;
    height: 100%;
    padding: 2px;
    color: white;
    font-size: 0.8em;
    text-shadow: -1px 0 1px black, 0 1px 1px black, 1px 0 1px black, 0 -1px 1px black;
    background-size: 80%;
    background-repeat: no-repeat;
    background-position: center;
    background-origin: border-box;
    flex-grow: 0;
    flex-shrink: 0;
    min-height: 0;
    white-space: normal;
    line-height: 0.7em;
    font-weight: bold;
}
::slotted([value]:hover) {
    background-size: 100%;
}
::slotted([value].mark) {
    color: #54ff54;
}
`);

function getAlign(value) {
    switch (value) {
        case "start":
            return "flex-start";
        case "end":
            return "flex-end";
        default:
            return "center";
    }
}

export default class ItemKey extends StateDataEventManager(CustomElement) {

    constructor() {
        super();
        this.shadowRoot.append(TPL.generate());
        STYLE.apply(this.shadowRoot);
        /* --- */
        this.registerStateHandler("value", event => {
            this.value = event.data;
        });
        this.registerStateHandler("type", event => {
            this.fillItemChoices();
        });
        this.addEventListener("click", event => this.next(event));
        this.addEventListener("contextmenu", event => this.prev(event));
    }

    connectedCallback() {
        super.connectedCallback();
        // state
        const state = this.getState();
        if (state != null) {
            this.value = state.value;
            this.fillItemChoices();
        }
    }

    disconnectedCallback() {
        super.disconnectedCallback();
    }

    get ref() {
        return this.getAttribute("ref");
    }

    set ref(val) {
        this.setAttribute("ref", val);
    }

    get value() {
        return this.getAttribute("value");
    }

    set value(val) {
        this.setAttribute("value", val);
    }

    get readonly() {
        return this.getAttribute("readonly");
    }

    set readonly(val) {
        this.setAttribute("readonly", val);
    }

    get halign() {
        return this.getAttribute("halign");
    }

    set halign(val) {
        this.setAttribute("halign", val);
    }

    get valign() {
        return this.getAttribute("halign");
    }

    set valign(val) {
        this.setAttribute("valign", val);
    }

    static get observedAttributes() {
        return ["ref", "value", "halign", "valign"];
    }
    
    attributeChangedCallback(name, oldValue, newValue) {
        if (oldValue != newValue) {
            switch (name) {
                case "ref":
                    {
                        // state
                        const state = ItemStates.get(this.ref);
                        this.switchState(state);
                        if (state != null) {
                            if (this.isConnected) {
                                this.value = state.value;
                            }
                            const data = state.props;
                            // settings
                            if (data.halign != null) {
                                this.halign = data.halign;
                            }
                            if (data.valign != null) {
                                this.valign = data.valign;
                            }
                            this.fillItemChoices();
                        }
                    }
                    break;
                case "halign":
                    this.shadowRoot.getElementById("slot").style.setProperty("--halign", getAlign(newValue));
                    break;
                case "valign":
                    this.shadowRoot.getElementById("slot").style.setProperty("--valign", getAlign(newValue));
                    break;
                case "value":
                    {
                        const activeEl = this.querySelector(".active");
                        if (activeEl != null) {
                            activeEl.classList.remove("active");
                        }
                        const newEl = this.querySelector(`[value="${newValue}"]`);
                        if (newEl != null) {
                            newEl.classList.add("active");
                        }
                    }
                    break;
            }
        }
    }

    fillItemChoices() {
        this.innerHTML = "";

        const state = ItemStates.get(this.ref);
        const data = state.props;
        if (!data) return;

        for (let i = 0; i <= state.max; ++i) {
            let img = data.images;
            if (Array.isArray(img)) {
                img = img[i];
            }
            const opt = createOption(i, img, data, state.max);
            if (i == this.value) {
                opt.classList.add("active");
            }
            this.append(opt);
        }
    }

    next(event) {
        if (!this.readonly) {
            const state = this.getState();
            if (state != null) {
                const data = state.props;
                const oldValue = state.value;
                let value = oldValue;
                if ((event.shiftKey || event.ctrlKey)) {
                    if (data.alternate_counting) {
                        for (let i = 0; i < data.alternate_counting.length; ++i) {
                            let alt = parseInt(data.alternate_counting[i]);
                            if (isNaN(alt)) {
                                alt = 0;
                            }
                            if (alt > oldValue) {
                                value = data.alternate_counting[i];
                                break;
                            }
                        }
                    } else {
                        value = parseInt(data.max);
                    }
                } else {
                    value++;
                }
                if (value != oldValue) {
                    state.value = value;
                }
            }
        }
        if (!event) return;
        event.preventDefault();
        return false;
    }

    prev(event) {
        if (!this.readonly) {
            const state = this.getState();
            if (state != null) {
                const data = state.props;
                const oldValue = state.value;
                let value = oldValue;
                if ((event.shiftKey || event.ctrlKey)) {
                    if (data.alternate_counting) {
                        for (let i = data.alternate_counting.length - 1; i >= 0; --i) {
                            let alt = parseInt(data.alternate_counting[i]);
                            if (isNaN(alt)) {
                                alt = data.max;
                            }
                            if (alt < parseInt(oldValue)) {
                                value = data.alternate_counting[i];
                                break;
                            }
                        }
                    } else {
                        value = 0;
                    }
                } else {
                    value--;
                }
                if (value != oldValue) {
                    state.value = value;
                }
            }
        }
        if (!event) return;
        event.preventDefault();
        return false;
    }

}

UIRegistry.get("item").register("key", ItemKey);
customElements.define("ootrt-itemkey", ItemKey);

function createOption(value, img, data, max_value) {
    const opt = document.createElement("emc-option");
    opt.value = value;
    opt.style.backgroundImage = `url("${img}"`;
    if (value == 0 && !data.alwaysActive) {
        opt.style.filter = "contrast(0.8) saturate(0.5) brightness(0.4)";
    }
    if (data.counting) {
        if (Array.isArray(data.counting)) {
            opt.innerHTML = data.counting[value];
        } else {
            if (value > 0 || data.alwaysCounting) {
                if (data.showMax) {
                    opt.innerHTML = `${value} / ${max_value}`;
                } else {
                    opt.innerHTML = value;
                }
            }
        }
        if (data.mark !== false) {
            const mark = parseInt(data.mark);
            if (value >= max_value || !isNaN(mark) && value >= mark) {
                opt.classList.add("mark");
            }
        }
    }
    // radial-gradient(ellipse at center, rgb(24, 241, 21) 0%,rgb(24, 241, 21) 45%,rgba(0,255,255,0) 72%,rgba(0,255,255,0) 87%)
    return opt;
}
