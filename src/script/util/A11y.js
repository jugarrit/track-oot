import StyleVarSettingsHandler from "./StyleVarSettingsHandler.js";

// register a11y colors
new StyleVarSettingsHandler("location_status_opened_color", "location-status-opened-color");
new StyleVarSettingsHandler("location_status_available_color", "location-status-available-color");
new StyleVarSettingsHandler("location_status_unavailable_color", "location-status-unavailable-color");
new StyleVarSettingsHandler("location_status_possible_color", "location-status-possible-color");
